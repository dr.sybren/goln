package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"time"

	"github.com/mattn/go-colorable"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/dr.sybren/goln/sysinfo"
)

var cliArgs struct {
	quiet, debug, trace bool
	force               bool

	source      string
	destination string
}

func main() {
	output := zerolog.ConsoleWriter{Out: colorable.NewColorableStdout(), TimeFormat: time.RFC3339}
	log.Logger = log.Output(output)

	parseCliArgs()

	osDetail, err := sysinfo.Description()
	if err != nil {
		osDetail = err.Error()
	}
	configLogLevel()

	log.Info().
		Str("os", runtime.GOOS).
		Str("osDetail", osDetail).
		Str("arch", runtime.GOARCH).
		Int("pid", os.Getpid()).
		Msg("starting goln")

	can, err := sysinfo.CanSymlink()
	if err != nil {
		log.Fatal().Err(err).Msg("error checking policies")
	}
	if !can {
		log.Fatal().Msg("it seems this system cannot use symlinks")
	}

	dest := cliArgs.destination
	destStat, err := os.Stat(dest)
	switch {
	case errors.Is(err, os.ErrNotExist):
		// This is expected.
	case destStat.IsDir():
		// The symlink should get the name of the link target.
		_, srcFile := filepath.Split(cliArgs.source)
		dest = filepath.Join(dest, srcFile)
	case err != nil:
		log.Fatal().Err(err).Msg("destination path has issues")
	}

	logger := log.With().
		Str("from", cliArgs.source).
		Str("to", dest).
		Logger()

	logger.Info().Msg("creating symlink")

	err = os.Symlink(cliArgs.source, dest)

	if os.IsExist(err) && cliArgs.force {
		logger.Info().Msg("destination exists, removing and trying again")
		if rmErr := os.Remove(dest); rmErr != nil {
			logger.Fatal().Err(rmErr).Msg("error deleting destination")
		}
		err = os.Symlink(cliArgs.source, dest)
	}
	if err != nil {
		log.Fatal().
			Err(err).
			Str("from", cliArgs.source).
			Str("to", dest).
			Msg("error making symlink")
	}

	log.Info().Msg("symlink OK")
}

func parseCliArgs() {
	flag.BoolVar(&cliArgs.quiet, "quiet", false, "Only log warning-level and worse.")
	flag.BoolVar(&cliArgs.debug, "debug", false, "Enable debug-level logging.")
	flag.BoolVar(&cliArgs.trace, "trace", false, "Enable trace-level logging.")
	flag.BoolVar(&cliArgs.force, "force", false, "Delete destination before creating the symlink, if the destination already exists.")
	flag.Parse()

	cliArgs.source = flag.CommandLine.Arg(0)
	cliArgs.destination = flag.CommandLine.Arg(1)

	if cliArgs.source == "" {
		usage()
		log.Fatal().Msg("provide source and destination files")
	}

	if cliArgs.destination == "" {
		cliArgs.destination = "."
	}

}

func configLogLevel() {
	var logLevel zerolog.Level
	switch {
	case cliArgs.trace:
		logLevel = zerolog.TraceLevel
	case cliArgs.debug:
		logLevel = zerolog.DebugLevel
	case cliArgs.quiet:
		logLevel = zerolog.WarnLevel
	default:
		logLevel = zerolog.InfoLevel
	}
	zerolog.SetGlobalLevel(logLevel)
}

func usage() {
	flag.Usage()

	out := flag.CommandLine.Output()
	fmt.Fprintf(out, "  source\n")
	fmt.Fprintf(out, "\tPath to link from.\n")
	fmt.Fprintf(out, "  destination\n")
	fmt.Fprintf(out, "\tPath to link to. If ommitted, use the current working directory.\n")
	fmt.Fprintf(out, "\n")
	fmt.Fprintf(out, "The symbolic link will be created at 'destination' and point to 'source'.\n")
}
